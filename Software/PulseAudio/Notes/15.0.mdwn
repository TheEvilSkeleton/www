[[!inline pages="Software/PulseAudio/TOC" quick="yes" raw="yes"]]

# PulseAudio 15.0 release notes

[[!toc startlevel=2 levels=3]]

## Notes for end users

### Support for LDAC and AptX bluetooth codecs, plus "SBC XQ" (SBC with higher-quality parameters)

PulseAudio now supports the LDAC and AptX codecs for A2DP. In order to use them, the device has to support the codecs too. Also "XQ" configuration variants for the old SBC codec are available. The SBC XQ variants have a fixed (and higher than the "normal" SBC) bitrate, so they may have more drop-outs if the wireless connection is bad, but on the other hand the quality is always good if the connection is good. For variable bitrate, pulseaudio is now able to increase the bitrate again after it was reduced due to connectivity issues. 

Switching the codec can be done on the command line with `pactl send-message /card/bluez_card.XX_XX_XX_XX_XX_XX/bluez switch-codec '"CODEC"'` (note the double quotes inside single quotes, this is needed because the server expects the double quotes and without the single quotes the shell would remove the double quotes before the string gets sent to the server). Replace `XX_XX_XX_XX_XX_XX` with the device address, which can be found for example with `LANG=C pactl list cards | grep Name`. Replace `CODEC` with one of the available codec names: `aptx`, `aptx_hd`, `ldac_hq` ("High Quality"), `ldac_sq` ("Standard Quality"), `ldac_mq` ("Mobile Quality"), `sbc`, `sbc_xq_453`, `sbc_xq_512`, `sbc_xq_552`. A list of codecs supported by the device can be obtained with `pactl send-message /card/bluez_card.XX_XX_XX_XX_XX_XX/bluez list-codecs` and the currently active codec can be queried with `pactl send-message /card/bluez_card.XX_XX_XX_XX_XX_XX/bluez get-codec`. A simplified way of switching codecs is already implemented in pavucontrol and will be provided with the next pavucontrol release.

### Support for HFP bluetooth profiles

Native support for HFP AG role has been added. The native backend has become the default backend for HSP/HFP. Support for ofono is still available when setting the backend parameter to `auto` or `ofono`. The native backend currently only implements the HFP AG role, but not the HFP HS role, therefore ofono is still required for this role. Additionally, support for mSBC has been implemented to improve voice quality. 

### Support for Bluetooth A2DP AVRCP Absolute Volume

AVRCP Absolute Volume allows PulseAudio to control the volume of the connected A2DP device (when playing to it) or for a connected device to control volume in PulseAudio (when the device plays to PulseAudio). Previously when playing to e.g. a headset, PulseAudio did its own volume control in software, and the headset did its own volume in hardware. Having two levels of volumes sometimes made it difficult to get high enough volume, now there's only one level of volume control.

This feature can be disabled by passing `avrcp_absolute_volume=false` to module-bluetooth-discover in the `default.pa` configuration file.

### ALSA path configuration files can now be placed in user home directory

The code that loads the ALSA path configuration files now checks if the files exist in the directories specified with the `XDG_DATA_HOME` or `XDG_DATA_DIRS` environment variables (under `pulseaudio/alsa-mixer/paths` subdirectory). Those environment variables are defined by the [XDG Base Directory Spec](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html), and even if those environment variables aren't set, the XDG Base Directory Spec defines default locations that PulseAudio uses. In particular, in the usual case where `XDG_DATA_HOME` isn't set, the default value is `$HOME/.local/share`, so PulseAudio will look for path configuration files from that directory under the user's home. This is useful when it's necessary to customize the path configuration files. Previously the files in `/usr/share/pulseaudio/alsa-mixer/paths` had to be modified, and the modifications were lost whenever upgrading PulseAudio.

### module-virtual-surround-sink rewritten

The convolution processing code has been rewritten to use a blocked FFT function, supporting potentially much larger impulse responses. Existing impulses will continue to function with the rewrite. The filter optionally supports asymmetrical responses, which may be enabled by specifying two sets of impulse files, using the `hrir_left` and `hrir_right` options instead of the original `hrir` option. Impulses should be matching format and channel layouts. The channel layout of the actual audio input for processing may be specified using the `channel_map` setting, so long as all specified channels are present in one or both impulses.

### More options for module-jackdbus-detect

module-jackdbus-detect automatically loads a JACK sink and source when JACK starts. It's now possible to better configure the parameters of the sink and source. module-jackdbus-detect accepts these new arguments:

 * `sink_name` - Name for the JACK sink
 * `source_name` - Name for the JACK source
 * `sink_properties` - Additional properties to set on the JACK sink
 * `sink_channel_map` - Channel map for the JACK sink
 * `source_channel_map` - Channel map for the JACK source
 * `source_properties` - Additional properties to set on the JACK source
 * `sink_client_name` - The name that the JACK sink uses when it registers a client with JACK
 * `source_client_name` - The name that the JACK source uses when it registers a client with JACK

### Improved hardware support

#### SteelSeries Arctis 9

The same custom configuration that is used with several other USB gaming headsets is now applied to SteelSeries Arctis 9, which makes the device much more functional.

#### HP Thunderbolt Dock 120W G2

HP Thunderbolt Dock 120W G2 has a 3.5 mm headset connector, and there's a separate audio module sold for the dock that provides speakerphone functionality (integrated microphone and speaker with echo cancellation). PulseAudio now has configuration that improves the device descriptions shown to the user and marks the speakerphone as a preferred device for voice audio.

#### Behringer U-Phoria UMC22

Behringer U-Phoria UMC22 has two mono microphone inputs, which show up as a single stereo device in ALSA. PulseAudio now has custom configuration for this sound card, allowing the user to select a profile that enables only one or the other of the channels. That way recording applications don't end up recording a stereo track that has audio only on one channel.

#### OnePlus Type-C Bullets

OnePlus Type-C Bullets is a simple USB-C headset device. It works with the default configuration too, but custom configuration was added to hide card profiles that don't make sense (for example, HDMI showed up as a supported profile with the default configuration).

#### Sennheiser GSX 1000/1200 PRO

Sennheiser GSX 1000 and 1200 PRO are USB DACs designed for gaming. They have a separate mono output for voice audio, and they also provide a virtual surround device that downmixes 7.1 surround audio to headphones in hardware. Custom configuration was added to expose these devices properly.

### New udev variable: PULSE_MODARGS

It's now possible to set module-alsa-card module arguments via udev configuration by using the PULSE_MODARGS variable. This allows for example setting `tsched=false` for a particular sound card that has trouble with the timer-based scheduling. Previously the options were to pass the tsched option to module-udev-detect in default.pa, causing all sound cards to be affected, or to give up automatic sound card detection and hotplug support altogether and configure each sound card manually in default.pa.

### max_latency_msec argument added to module-null-source

It's now possible to configure the upper limit for latency of module-null-source with the `max_latency_msec` module argument.

### module-filter-apply can take filter parameters from device properties

module-filter-apply automatically loads a filter module when a stream wants to be filtered (i.e. has the `filter.want` property). Previously additional filter module arguments could be provided by the stream in the `filter.apply.<filter>.paramaters` property, now it's possible to set `filter.apply.<filter>.parameters` also in the hardware sink/source properties, so a stream can simply request a filter without specifying the parameters, and the hardware device can set the (possibly hardware specific) parameters.

### module-match can now be loaded multiple times

The original expectation was that there wouldn't be need for loading module-match multiple times, so it was marked as a module that isn't meant to (and can't) be loaded multiple times. However, it turned out to be useful to match against different properties with different configuration files, so the module can now be loaded multiple times.

### Improvements to FreeBSD support

Several patches from FreeBSD have been upstreamed. Unplugging sound cards should behave better now, devices have better descriptions, hotplugging devices is now supported with module-devd-detect.

### Windows support added to Meson

PulseAudio's Meson build system was lagging behind the old Autotools build system when it came to building for Windows, now it should be possible to build PulseAudio for Windows with Meson using MinGW. There are also various fixes for Windows and PA can now be run as a service on Win32.

### Additional commands for pactl

pactl now supports the following additional commands: `get-default-{sink|source}`, `get-{sink|source}-volume` and `get-{sink|source}-mute`

### Card profiles can be set to sticky

Card profiles can be set to sticky using the command `pactl send-message /card/<card name> set-profile-sticky 'true|false'`. The current status can be queried using `pactl send-message /card/<card name> get-profile-sticky`. If a card profile is sticky, it will always be restored, even if the profile is unavailable. Also port availability changes on the card will not impact the profile choice. This is for example useful to set the card profile permanently to "off" for HDMI devices. Setting profiles to sticky is already implemented in pavucontrol and will be provided with the next pavucontrol release.

## Notes for application developers

### New API for sending messages from clients to PulseAudio objects

The new "message API" was added to facilitate easy communication between clients and pulseaudio objects. It's currently used for switching bluetooth codecs and setting card profiles to sticky. The message API is using JSON format to exchange information between pulseaudio objects and a client.

### New mechanism for applications to disable shared memory on their connection to PulseAudio

If an application wants last bits of virtual memory, it can forcefully disable shared memory communication channels by setting the `PA_PROP_CONTEXT_FORCE_DISABLE_SHM` boolean proplist value on the `pa_context` object when connecting. The primary user would be Wine 32bit with 2Gb vmem limit.

## Notes for packagers

### Autotools build system have been dropped

As already announced in the 14.0 release notes, the support for autotools finally has been dropped. The meson build system has been improved significantly and is now the standard way to build pulseaudio. 

### The startup script can now read additional configuration from the /etc/pulse/default.pa.d/ directory

PulseAudio's startup script (default.pa for the normal per-user mode and system.pa for the system mode) defines the set of modules to load automatically on startup. At the end of the scripts there is now an include directive that loads files from `/etc/pulse/default.pa.d/` or `/etc/pulse/system.pa.d/`. This is primarily meant for distributions when they want to add some module to the configuration when installing a package: it's now easy to install a file containing the configuration for the installed package. Users can of course use this mechanism too if they prefer to add a module to the configuration without modifying the original startup script.

### Option to build client library and utilities only

When PipeWire is used to replace PulseAudio, libpulse and PulseAudio utilities are still useful but the PulseAudio daemon is not needed. It's now possible to compile libpulse and the utilities without compiling the daemon. To do this, pass `-Ddaemon=false` to Meson.

### Avoid loading X11 modules on Wayland (GNOME-only for now)

PulseAudio traditionally loads a few X11 modules automatically on a graphical login via the `/etc/xdg/autostart/pulseaudio.desktop` file. That's not really desirable on Wayland, because that will autostart XWayland even if it's not otherwise used. The `pulseaudio.desktop` file now contains the line `X-GNOME-HiddenUnderSystemd=true`, so when using GNOME and systemd the X11 modules aren't loaded this way any more. If GNOME is run under X11, there's a new systemd service called `pulseaudio-x11` that handles loading the X11 modules.

This behaviour is not available on other desktop environments than GNOME, but now that it's implemented once, it should be pretty easy to develop a similar solution for other desktop environments as well.

### OSS support is now configurable in Meson

The `oss-output` Meson option can be used to explicitly enable or disable OSS support (previously, when using Meson instead of Autotools, OSS support was automatically enabled when the `sys/soundcard.h` header was found).

### Valgrind support is now configurable in Meson

The `valgrind` Meson option can be used to explicitly enable or disable Valgrind support (previously Valgrind support was automatically enabled when the `valgrind/memcheck.h` header was found).

## git shortlog

<pre>
Alexey Rubtsov (1):
      Translated using Weblate (Russian)

Alper Nebi Yasak (1):
      alsa-ucm: Log about the correct path value when probing volumes

Anders Jonsson (1):
      Translated using Weblate (Swedish)

Arun Raghavan (13):
      proplist-util: Drop deprecated G_CONST_RETURN macro
      glib-mainloop: Drop deprecated g_get_current_time() usage
      stream-restore,device-restore: Avoid unaligned access
      mutex-posix: Fix error assignment for unlock() check
      doc: Add some contributor guidelines
      Update NEWS
      Update NEWS
      gitignore: Add install/ for local prefixed installs
      build-sys: Bump libpulse soversion
      build-sys: Fix a couple of meson warnings
      Update NEWS for 15.0
      build-sys: Fix a warning related to gsettings and config data
      build-sys: Fix a warning related to avahi and config data

Ben Buchwald (1):
      jackdbus-detect: Passthrough more arguments to sink and source

Benjamin Valentin (1):
      alsa-mixer: disable has_dB if max_dB is negative

Carlos Garnacho (3):
      daemon: Add "stop" argument to start-pulseaudio-x11 script
      daemon: Install start-pulseadio-x11 as a Xwayland-session.d script
      daemon: Rely on systemd unit file for X11 plugin initialization

Carmen Bianca Bakker (2):
      Added translation using Weblate (Esperanto)
      Translated using Weblate (Esperanto)

Christopher Arndt (1):
      Add port order metadata to JACK sink / source ports

Christopher Snowhill (1):
      virtual-surround-sink: Use FFTW3 instead of naive approach

David (2):
      Capitalize jack in module-jack-source.c
      Capitalize jack in module-jack-sink.c

Dusan Kazik (1):
      Translated using Weblate (Slovak)

Edward Lee (9):
      win32: Handle (WSA)EWOULDBLOCK as EAGAIN.
      win32: Fix (interim) esound paths in module-protocol-stub.c
      win32: Check WSAGetLastError() in pa_is_unix_socket_stale.
      win32: (Temporarily) Add sockaddr_un definition from <afunix.h>
      win32: Add support for Unix sockets in autoconf build.
      win32: Change linkage of lt_preloaded_symbols to match definition in libtool in daemon/main.c.
      win32: Fix meson build system for Windows.
      Win32: Fix build issues.
      Win32: Use WAVEFORMATEX for multi-channel audio output on Windows.

Emilio Herrera (1):
      Translated using Weblate (Spanish)

Evan Miller (1):
      Fix a strict-prototypes warning

Fabian Affolter (1):
      Translated using Weblate (German)

Faidon Liambotis (2):
      tests: fix use of uninitialized variable cpu_info
      buildsys/autotools: add pulseaudio-x11.service.in

Felipe Sateler (3):
      treewide: fix a bunch of typos
      Don't link libpulsecommon with GLib and GTK
      tests: fix json test for large numbers

Frédéric Danis (1):
      core, modules: Remove useless EINTR tests

Georg Chini (17):
      protocol-native: add message sending capability
      pactl, pacmd, cli-command: Add send-message command
      core: add message handler
      pactl: Implement list message-handlers
      message-params: Allow parameter strings to contain escaped curly braces
      core-util: Add pa_atoi64() and pa_atou64() functions
      message-params: Add read/write functions for various simple data types
      message-params: Add read functions for arrays
      message-params: Fix array reading functions
      loopback: Fix sink latency calculation if source is alsa source with fixed latency
      sink: Unlink monitor source before unlinking sink
      null-source: Change max_rewind when the latency changes
      source-output: Fix source-output volume after move of virtual source stream
      alsa-sink: Do not increase watermark when requested to rewind 0 bytes
      loopback: Fix crash bug
      stream-restore: Fix use of uninitialized variable
      null-sink: Request no more than the configured latency from sink-input

Greg V (19):
      build-sys: meson: set HAVE_DLADDR even if it's in libc (FreeBSD), not libdl
      build-sys: meson: when libc iconv is used, tell libiconv header to pretend to be libc's iconv
      build-sys: meson: check for shm_open even if it's in libc (FreeBSD), not librt
      build-sys: meson: add missing pthread dependency on alsa tests
      padsp: fix build on FreeBSD (use same ioctl signature as glibc)
      raop: add missing netinet include on FreeBSD
      build-sys: meson: add execinfo dependency for FreeBSD
      build-sys: meson: support memfd without SYS_memfd_create (FreeBSD 13)
      iochannel: support credentials passing on FreeBSD
      pulsecore: remove ancient __FreeBSD_version < 600000 code
      util: implement pa_get_binary_name on FreeBSD without procfs requirement
      detect: fix/improve FreeBSD support
      oss: do not fall back to write() mode on FreeBSD
      oss: support 24-bit sample formats
      oss: support hot-unplug on FreeBSD
      oss: parse device names correctly on FreeBSD
      module-devd-detect: new module for soundcard hotplug on FreeBSD
      atomic-test: unbreak on FreeBSD (cpuset_t)
      build-sys: meson: only warn about lack of udev on Linux

Göran Uddeborg (4):
      Translated using Weblate (Swedish)
      Translated using Weblate (Swedish)
      Translated using Weblate (Swedish)
      Translated using Weblate (Swedish)

Hela Basa (1):
      Added translation using Weblate (Sinhala)

Henri Chain (1):
      systemd: move to session slice

Hui Wang (8):
      alsa-mixer: disable the Auto-Mute once the system has speaker
      switch-on-port-available: Switch to headset or mic on unknown avail
      alsa-card: add dynamic priority bonus base for alsa profiles
      switch-on-port-available: checking the off profile when switching profile
      alsa-mixer: increase the max number of HDMI/DP output device to 11
      Revert "alsa-mixer: Set "Front" control to 0 dB on headphone path"
      alsa-mixer: only use switch to mute Front in the Headphone path
      alsa-sink/source: set volume to hw immediately if ucm_port changing

Igor V. Kovalenko (88):
      pa-info: look for alsa-info.sh in /usr/sbin as well
      build-sys: meson: add oss-output option for OSS output support
      build-sys: meson: use target_machine.cpu_family() for CANONICAL_HOST
      message-params: add read length param reference to array read methods
      message-params: consume array enclosing {} by array read methods
      module-alsa-card: handle udev PULSE_MODARGS
      build-sys: meson: change oss-output boolean to Meson feature
      bluetooth: align max renderable block size to frame size
      context: add property to forcefully disable shared memory channels
      ladspa-sink: do not call pa_sink_suspend if PA_SINK_IS_LINKED is false
      x11: gracefully handle X11 connection error
      x11: gracefully handle ICE connection error
      daemon: fix default daemon.conf when running from build tree
      database: extract common method to handle machine id and architecture
      database: use existing database matching same architecture prefix
      database: drop arch from newly created database file name
      database: pick old database file from any arch
      database: clean up remaining references to CANONICAL_HOST
      alsa-mixer: add support for SteelSeries Arctis 9
      buildsys: autotools: move map-file to src/pulse
      buildsys: meson: implement update-map-file target
      map-file: regenerate
      bluetooth: Make GStreamer threads realtime
      bluetooth: allow increasing output bitrate
      bluetooth: allow increasing SBC output bitrate
      bluetooth: pass output_rate_refresh_interval_ms module parameter
      bluetooth: a2dp dual channel SBC XQ codec configurations
      bluetooth: support increasing bitrate for SBC XQ
      build-sys: drop gconf support
      build-sys: meson: add tcp-wrappers
      build-sys: meson: support elogind alternative for module-systemd-login
      bluetooth: remove unwanted change from bluez fallback code
      bluetooth: enable module-bluez5-discover argument enable_native_hfp_hf
      bluetooth: fix headset=auto ofono handover
      bluetooth: prefer headset HFP HF connection with native backend
      bluetooth: complete bluetooth profile separation
      bluetooth: use device flag to prevent assertion failure during shutdown
      bluetooth: rename enable_hs_role to enable_shared_profiles
      bluetooth: clean up rfcomm_write usage
      bluetooth: fix typo checking if target codec is available
      bluetooth: correct rfcomm command and reply formatting
      build-sys: drop autotools build system
      build-sys: meson: fix meson dist version
      bluetooth: accept +VGM/+VGS unsolicited reply with '=' and ':' separator
      json: improve supported numbers
      json: add JSON encoder
      message-params: use JSON instead of custom format
      message-params: clean up
      bluetooth: update messaging api reference
      bluetooth: prioritize native backend HFP HF connection
      bluetooth: unify encoder code paths
      bluetooth: unify decoder code paths
      bluetooth: add CVSD codec implementation
      bluetooth: apply write MTU detection based on read packet size
      bluetooth: use helper to set multiple transport object attributes
      bluetooth: mSBC: ignore all-zero packets
      bluetooth: mSBC: ignore empty encoded frame
      bluetooth: prepare to redo transport writeout scheduling
      bluetooth: add mSBC to backend-ofono
      bluetooth: show negotiated HFP codec
      bluetooth: remember negotiated HFP codec in native backend
      bluetooth: produce silence on mSBC decoding error
      bluetooth: add more call indicators
      bluetooth: set initial packet size to 60
      bluetooth: split BT codec from A2DP SEP configuration api
      bluetooth: add modarg to allow disabling mSBC codec
      bluetooth: mSBC: log lost input audio packets at debug level
      bluetooth: handle HFP codec list in any order
      bluetooth: disable HSP HS profile by default
      alsa-mixer: Allow selected fallback mappings in all profile sets
      build-sys: meson: allow building client libraries only
      build-sys: meson: do not install manpages for disabled parts
      bluetooth: adjust sync to source with different sample spec
      build-sys: meson: only require libltdl if daemon is built
      build-sys: meson: make doxygen optional
      alsa-mixer: Add support for usb audio in the Dell dock WD19
      alsa-mixer: prevent double-free on decibel fixes object key
      build-sys: meson: restore big endian detection
      json: remove json from public API
      build-sys: meson: check HAVE_MMX and HAVE_SSE before calling initializers
      alsa-mixer: check if mapping is NULL before using it
      card: handle sticky profile flag
      doc: link to site documentation for IRC channels
      alsa-mixer: updated Sennheiser GSX 1200 device ID
      build-sys: meson: require GIO dependency for RTP-GStreamer
      build-sys: meson: Require bluez dependency if bluez5 feature is enabled
      module-gsettings: Handle I/O hangup
      alsa-ucm: fix persistent port names with alsa-lib >= 1.2.5

Ilja van Sprundel (1):
      webrtc: Avoid using dynamic strings as format strings

Jaechul Lee (3):
      null-sink: Change block size to 50 msec when norewinds is set
      null-source: Add max_latency_msec argument
      null-source: Fix max_latency_msec argument is not changed

James Bottomley (6):
      bluetooth: use consistent profile names
      bluetooth: separate HSP and HFP
      bluetooth: add correct HFP rfcomm negotiation
      bluetooth: make native the default backend
      bluetooth: add support for mSBC codec
      bluetooth: add wideband audio codec negotiation to HFP

Jan Alexander Steffens (heftig) (1):
      build-sys: meson: Add missing include dir

Jan Kuparinen (6):
      Translated using Weblate (Finnish)
      Translated using Weblate (Finnish)
      Translated using Weblate (Finnish)
      Translated using Weblate (Finnish)
      Translated using Weblate (Finnish)
      Translated using Weblate (Finnish)

Jaroslav Kysela (3):
      alsa: mixer - use safe dB range values when the override mask is unset
      alsa: mixer - add support up 8 mixer channels
      alsa: fix the plug: PCM device name creation

Jason Nader (9):
      man: clarify pactl command arguments
      pactl: add `get_default_sink` command
      pactl: add `get-sink-volume` command
      pactl: add `get_default_source` command
      pactl: add `get_source_volume` command
      Apply 1 suggestion(s) to 1 file(s)
      Apply 1 suggestion(s) to 1 file(s)
      Apply 1 suggestion(s) to 1 file(s)
      Apply 3 suggestion(s) to 1 file(s)

Johannes Wolf (1):
      alsa-profiles: Add NI Komplete Audio 6 profiles

Julien Humbert (1):
      Translated using Weblate (French)

Kai-Heng Feng (3):
      alsa-mixer: Add support for HP Thunderbolt Dock
      alsa-mixer: Handle the index for ALSA mixer jack identifiers
      alsa-mixer: Support dual Front Headphone Jack

Karl Ove Hufthammer (2):
      Translated using Weblate (Norwegian Nynorsk)
      Translated using Weblate (Norwegian Nynorsk)

Klaas van Schelven (1):
      pactl: add `get-(source|sink)-mute` commands

Laurent Bigonville (2):
      tests: Fix incompatible pointer type on Debian kfreebsd
      iochannel: Fix FTBFS on Debian kfreebsd

Laurențiu Nicola (1):
      alsa-mixer: Add OnePlus Bullets udev rule

Lyndon Brown (1):
      proplist: tweak documentation for `pa_proplist_get()`

Marijn Suijten (31):
      bluetooth/bluez5-util: Do not reply to ClearConfiguration
      bluetooth: Check support for encoding and decoding separately
      bluetooth: Add missing can_be_supported check to is_codec_available
      bluetooth/gst: Add assertions around element_add_pad and bin_add
      bluetooth/gst: Determine PA input/output caps in generic code
      bluetooth/aptx: Deduplicate caps setup for encoding and decoding
      bluetooth/aptx: Use capsfilter instead of appsink/appsrc "caps" prop
      bluetooth/gst: Move common enc/dec initialization back to generic init
      bluetooth: improve indentation around dbus_message_iter_open_container
      bluetooth: Unify BlueZ macro defines shared between util and native
      bluetooth: Consistently use BlueZ defines for strings
      treewide: Use DBus defines instead of direct string literals
      tests: Update ladspa-dbus to Python 3
      bluetooth: Remove unused ts_elapsed variable
      bluetooth/aptx: Simplify lifetime of `caps`
      bluetooth/aptx: Call the non-HD encode_buffer function
      bluetooth/gst: Unify encoder and decoder pipeline setup
      bluetooth: switch_codec should start with pa_blueooth_device_
      bluetooth: Move HSP gain conversions into backend-native
      bluetooth: backend-native: Round volume to closest instead of up
      bluetooth: Generalize speaker/microphone naming to sink/source
      bluetooth: Move attenuation decision to shared function
      bluetooth: Perform software attenuation until HF/HS reports gain control
      bluetooth: Set up hardware gain control if init volume is received late
      bluetooth: Only use hardware volume callbacks for peer attenuation
      bluetooth: Move HSP_MAX_GAIN to header for reuse in n_volume_steps
      bluetooth: Update source software volume on AVRCP SetAbsoluteVolume
      bluetooth: Synchronize AVRCP Absolute Volume with A2DP sink
      bluetooth: Report a2dp_source volume changes to the source device
      bluetooth: Delay A2DP Absolute Volume setup until property is available
      bluetooth: Add avrcp_absolute_volume module flag for disablement

Martin Wilck (2):
      parecord: fix "Failed to open audio file" for FLAC and OGG
      parecord: really fix recording OGG

Mattias Jernberg (1):
      alsa-mixer: Enable volume control of SteelSeries Arctis stereo output

Milo Casagrande (2):
      Translated using Weblate (Italian)
      Translated using Weblate (Italian)

Nazar Mokrynskyi (1):
      alsa-mixer: add mono input support for Behringer UMC22

Oğuz Ersen (2):
      Translated using Weblate (Turkish)
      Translated using Weblate (Turkish)

Patrick Gaskin (16):
      cli: Fix crash when using .include with an empty directory
      waveout: Fix compile warnings
      client: Make auto_connect_localhost respect HAVE_IPv6 and OS_IS_WIN32
      win32: Fix WSAStartup issues
      win32: Fix duplicate definitions in arpa-inet.h, arpa-inet.c, and poll.h
      win32: Use __MINGW_PRINTF_FORMAT instead of __printf__ for MinGW builds
      win32: Fix format specifiers for DWORD values
      win32: Fix privlibdir for running on Windows
      build-sys: meson: Merge system_conf config data from default_conf
      cli-command: Replace config dir with toplevel for .include on win32
      win32: Use DACLs when setting socket permissions
      win32: Fix minimum Windows version for inet_{ntop,pton}
      win32: Fix environment variables set with pa_{unset,set}_env not taking effect
      win32: Misc Unix socket fixes
      daemon: Add support for running as a service on win32
      win32: Add DACLs for directories created by system daemon

Patrick McLean (1):
      alsa-mixer: add support for Sennheiser GSX 1000 gaming DAC

Paul Seyfert (1):
      zsh-completion Add completion of sample names

Pierre Ossman (1):
      tunnel-sink-new: log buffer events

Piotr Drąg (5):
      i18n: Remove src/pulsecore/dllmain.c from POTFILES.in
      Translated using Weblate (Polish)
      Translated using Weblate (Polish)
      Translated using Weblate (Polish)
      Translated using Weblate (Polish)

Pjotr Vertaalt (5):
      Translated using Weblate (Dutch)
      Translated using Weblate (Dutch)
      Translated using Weblate (Dutch)
      Translated using Weblate (Dutch)
      Translated using Weblate (Dutch)

Ricky Tigg (5):
      Translated using Weblate (Finnish)
      Translated using Weblate (Finnish)
      Translated using Weblate (Finnish)
      Translated using Weblate (Finnish)
      Translated using Weblate (Finnish)

Robin Lahtinen (1):
      Translated using Weblate (Finnish)

Samuel Thibault (1):
      alsa-sink: increase watermark when there is nothing to rewind

Sanchayan Maity (19):
      Add default.pa.d folder support
      module-match: Allow loading module more than once
      bluetooth: a2dp-codec-util: Use lower index for higher priority
      bluez5-util: Add support for using RegisterApplication D-Bus API
      bluetooth: Support A2DP codec switching via messaging API
      bluetooth: Add a generic GStreamer codec module
      bluetooth: Add LDAC support via GStreamer
      bluetooth: Add aptX support via GStreamer
      bluetooth: Add support for getting list of negotiated codecs
      bluetooth: Add currently active codec name to card/sink/source proplist
      bluetooth: Add support for getting current active codec
      bluetooth: Introduce a can_be_supported API for A2DP codecs
      bluetooth: Prevent registration of an unavailable codec at runtime
      bluetooth: Register an endpoint according to encode or decode support
      bluetooth: Move codec specific bits to their own respective files
      bluetooth: Fix unregistering of the message handler
      bluetooth: ldac: Use format as FLOAT32LE
      bluetooth: ldac: Fix missing check for RTP LDAC payloader element
      bluetooth: aptX: Improve error message for when aptX element is not found

Scott Worley (1):
      gsettings: Emit glib logs on stderr, not stdout

Sebastian Krzyszkowiak (1):
      filter-apply: Look for filter parameters also in device properties

SimonP (1):
      alsa-mixer: Respect XDG base directory spec when loading path configs

Takashi Sakamoto (1):
      udev: fix match expression to Focusrite Saffire Pro i/o series for ignorance

Tanu Kaskinen (32):
      raop-sink: Don't set device.intended_roles=music
      mutex-posix: Log pthread_mutex_unlock() error
      main: set LC_NUMERIC to C
      alsa-mixer: Add support for the Center/LFE element
      alsa-mixer: Expand comments in the HP Thunderbolt Dock configuration
      core-util: Make range checks easier to read
      introspect: Add version check to pa_context_send_message_to_object()
      core-util: Reduce repetition in number parsing functions
      core-util: Never parse integers as octal
      core-util-test: Test parsing integer strings with leading zeros
      core-util-test: Drop "modargs" from test function names
      core-util-test: Test pa_atou64() and pa_atoi64()
      i18n: Update pulseaudio.pot
      module: Check version before loading a module
      build-sys: Add an option for enabling/disabling Valgrind
      pstream: Log "Cannot send block reference..." only once per pstream
      sink: Reduce chat sink priority
      alsa-mixer: Pick at most one fallback mapping
      sink: Increase JACK device priority
      alsa-mixer: Use unambiguous descriptions with dual headphone outputs
      alsa-mixer: Move HP EliteDesk 800 SFF/DM second headphone output to path analog-output-headphones-2
      alsa-mixer: Add support for the Headphone,1 element
      i18n: Update pulseaudio.pot
      switch-on-port-available: Pass correct port_pointers to switch_to_port()
      Update NEWS
      alsa-mixer: Accept negative max_dB when using decibel fixes
      i18n: Update .pot and .po files
      i18n: Update .pot and .po files
      sink-input, source-output: Add hooks for preferred device changes
      stream-restore: Fix NULL preferred device handling
      i18n: Update .pot and .po files
      stream-restore: make version check stricter when dropping old entries

Tobias Weise (1):
      Translated using Weblate (German)

Toni Estevez (2):
      Translated using Weblate (Spanish)
      Translated using Weblate (Spanish)

Yaron Shahrabani (6):
      Translated using Weblate (Hebrew)
      Translated using Weblate (Hebrew)
      Translated using Weblate (Hebrew)
      Translated using Weblate (Hebrew)
      Translated using Weblate (Hebrew)
      Translated using Weblate (Hebrew)

Yuri Chornoivan (2):
      Translated using Weblate (Ukrainian)
      Translated using Weblate (Ukrainian)

garrison (1):
      build-sys: meson: check if NEON code can be compiled on arm

morrishoresh (1):
      bluetooth: correct possible segmentation fault

pseyfert (1):
      fix handling of symbolic port names in zsh completion

scootergrisen (3):
      Translated using Weblate (Danish)
      Translated using Weblate (Danish)
      Translated using Weblate (Danish)

simmon (6):
      Translated using Weblate (Korean)
      Translated using Weblate (Korean)
      Translated using Weblate (Korean)
      Translated using Weblate (Korean)
      Translated using Weblate (Korean)
      Translated using Weblate (Korean)
</pre>
