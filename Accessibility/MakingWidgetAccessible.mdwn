For GTK3:

A minimal example of extending a base atk implementation is the GtkScaleAccessible class, based on GtkRangeAccessible, see gtk/gtk/a11y/gtkscaleaccessible.[ch]

For GTK4:

Custom widgets outside of GTK can provide a role, and can update their accessible name, label, and description
